package nbr1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Finder {

    public <T> int find(List<T> list, List<T> listToFind) {

        if (listToFind.isEmpty()) {
            return -1;
        }

        for (int index = 0; index < list.size(); index++) {
            T value1 = list.get(index);
            if (value1.equals(listToFind.get(0)) &&
                (subListMatch(list, listToFind, index))) {

                return index;
            }
        }

        return -1;
    }

    private <T> boolean subListMatch(List<T> list, List<T> listToFind, int index1) {

        if (list.size()-index1 >= listToFind.size()) {
            for (int index2 = 0; index2 < listToFind.size(); index2++) {
                if (list.get(index1++).equals(listToFind.get(index2))) {
                    if (index2 == (listToFind.size()-1)) { // last pos i list2 -> lists matched
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {

            System.out.print("Length of first list: ");
            int length = scanner.nextInt();
            List<String> listA = readListOfStrings(scanner, length);

            System.out.print("Length of second list: ");
            length = scanner.nextInt();
            List<String> listB = readListOfStrings(scanner, length);

            Finder finder = new Finder();
            System.out.println(String.valueOf(finder.find(listA, listB)));
        }
    }

    private static List<String> readListOfStrings(Scanner scanner, int length) {

        List<String> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(scanner.next());
        }
        return list;
    }
}
