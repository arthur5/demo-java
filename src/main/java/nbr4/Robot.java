package nbr4;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Robot {

    private final int[] positions;
    private final int MAX_HEIGHT;
    private final State state;

    private class State {
        int position = 0;
        boolean hasBlock;
    }

    public Robot(int positionCount, int maxHeight) {

        positions = new int[positionCount];
        MAX_HEIGHT = maxHeight;
        state = new State();
    }

    public String execute(String commands) {

        commands.chars().forEach(i -> applyCommand((char) i));
        return calcAllPositions();//  + "0";
    }

    private void applyCommand(char command) {

        switch (command) {

            // Pickup
            case 'P': {
                state.hasBlock = true;
                state.position = 0;
                break;
            }

            // Move
            case 'M': {
                if (state.position < positions.length-1) {
                    state.position++;
                }
                break;
            }

            // Lower
            case 'L': {
                if (state.hasBlock && positions[state.position] < MAX_HEIGHT) {
                    positions[state.position]++;
                    state.hasBlock = false;
                }
                break;
            }
        }
    }

    private String calcAllPositions() {

        return Arrays.stream(positions)
                .mapToObj(i -> convertToHexString(i))
                .collect(Collectors.joining());

    }

    private static String convertToHexString(int value) {
        return Integer.toHexString(value).toUpperCase();
    }

    private static void print(String s) {
        System.out.println(s);
    }

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {

            String commands = scanner.next();
            Robot robot = new Robot(10, 15);
            print(robot.execute(commands));
        }
    }
}
