package nbr2;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Counter {

    public String lookAndSay(int value) {
        return lookAndSay(Integer.toString(value));
    }

    private String lookAndSay(String digits) {

        StringBuilder result = new StringBuilder();
        String[] groups = digits.split("(?<=(.))(?!\\1)");

        for (String group : groups) {
            result.append(String.valueOf(group.length())).append(group.toCharArray()[0]);
        }

        return result.toString();
    }

    public String lookAndSay(int start, int n) {

        String result = lookAndSay(start);
        while(--n > 0) {
            result = lookAndSay(result);
        }
        return result;
    }

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {

            System.out.print("Start: ");
            int start = scanner.nextInt();
            System.out.print("Nbr of iterations: ");
            int n = scanner.nextInt();
            Counter counter = new Counter();
            System.out.println(counter.lookAndSay(start, n));
        }
    }
}
