package nbr3;

import java.util.Scanner;

public class Converter {

    String[] convTable = new String[]{"0", "a", "t", "l", "s", "i", "n"};

    public String convert(long value) {
        StringBuilder result = new StringBuilder();
        calculateConversion(result, value);
        return result.toString();
    }

    private void calculateConversion(StringBuilder result, long value) {

        long base, rest;

        base = value / 7;
        rest = value % 7;

        if (base >= 7) {
            calculateConversion(result, base);
        }
        addConvertedValue(result, base, rest);
    }

    private String addConvertedValue(StringBuilder sb, long base, long rest) {

        if (base >= 1 && base < 7) {
            sb.append(convTable[(int) base]);
        }
        sb.append(convTable[(int) rest]);
        return sb.toString();
    }

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {
            long input = scanner.nextLong();
            Converter converter = new Converter();
            System.out.println(converter.convert(input));
        }
    }
}
