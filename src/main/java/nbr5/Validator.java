package nbr5;

import java.util.*;

public class Validator {

	private static String FORMAT_ERROR = "0:0:0:format_error";

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {
			
			String input = scanner.next();
			Validator validator = new Validator();
			print(validator.validate(input));
		}

	}

	public String validate(String input) {

		int fieldNameCount = 0, recCount = 0, fieldCount = 0, emptyValueCount = 0;
		String lastFieldName = "[]";

		if (!validAsciiChars(input)) {
			return FORMAT_ERROR;
		}

		input = removeEscapedChars(input);

		// split to rows and count records
		String[] rows = input.split("~n");
		if (!verifyAllRows(rows)) {
			return FORMAT_ERROR;
		}
		recCount = rows.length-1;

		// process first line with field names
		String firstLine = rows[0];
		firstLine = cleanRow(firstLine);
		String[] fieldNames = splitFields(firstLine);

		fieldNameCount = fieldNames.length;
		lastFieldName = fieldNames[fieldNames.length-1];
		fieldCount = fieldNameCount;

		if (!verifyNoEmptyFieldsOrDuplicates(fieldNames)) {
			return FORMAT_ERROR;
		}

		// process all data rows
		for (int i=1; i < rows.length; i++) {

			String row = cleanRow(rows[i]);
			String[] dataFields = splitFields(row);

			if (dataFields.length > fieldCount) {
				fieldCount = dataFields.length;
				int additionalFieldCount = dataFields.length - fieldNameCount;
				lastFieldName = lastFieldName + "_" + String.valueOf(additionalFieldCount);
			}

			for (String field : dataFields) {
				if (field == null || field.isEmpty()) {
					emptyValueCount++;
				}
			}
		}

		String result = buildResult(recCount, fieldCount, emptyValueCount, lastFieldName);
		return result;
	}

	private boolean validAsciiChars(String input) {

		IntSummaryStatistics stats = input.chars().summaryStatistics();
		if (stats.getMax() > 126 || stats.getMin() < 32) {
			return false;
		}
		return true;
	}

	private String removeEscapedChars(String input) {

		input = input.replaceAll("~\\|", "");
		input = input.replaceAll("~~", "");
		return input;
	}

	private boolean verifyAllRows(String[] rows) {

		for (String row : rows) {
			if (! (row.startsWith("|") && row.endsWith("|"))) {
				return false;
			}
			else if (row.contains("~")) {
				return false;
			}
		}
		return true;
	}

	private boolean verifyNoEmptyFieldsOrDuplicates(String[] fieldNamesArray) {

		Set<String> fieldNames = new HashSet<>(Arrays.asList(fieldNamesArray));
		if ((fieldNames.size() != fieldNamesArray.length)
				|| (fieldNames.contains(""))) {
			return false;
		}
		return true;
	}

	private String[] splitFields(String row) {
		return row.split("\\|", -1);
	}

	private String cleanRow(String row) {
		row = row.replaceFirst("\\|$", "");
		row = row.replaceFirst("^\\|", "");
		return row;
	}

	private String buildResult(int recCount, int fieldCount, int emptyValueCount, String lastFieldName) {

		StringBuilder result = new StringBuilder();
		result.append(String.valueOf(recCount)).append(":")
				.append(fieldCount).append(":")
				.append(emptyValueCount).append(":")
				.append(lastFieldName);

		return result.toString();
	}

	private static void print(String s) {
		System.out.println(s);
	}
}