package nbr5;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class ValidatorTest {

    public static final String FORMAT_ERROR = "0:0:0:format_error";
    private Validator sut = new Validator();

    @Test
    public void returnsOutputWhenValidInput() {

        String input = "|name|address|~n|Patrick|patrick@test.com|~n|Annie||~n";
        String expected = "2:2:1:address";

        String actual = sut.validate(input);

        assertEquals("output", expected, actual);
    }

    @Test
    public void countsAdditionalFieldsAndAppendsNumberToLastFieldName() {

        String input = "|name|address|~n|Patrick|patrick@test.com|pat@test.com|pats address|~n" +
                "|Annie||annie@test.com|annies address|~n";
        String expected = "2:4:1:address_2";

        String actual = sut.validate(input);

        assertEquals("output with additional fields counted", expected, actual);
    }

    @Test
    public void returnsFormatErrorWhenInvalidInput() {

        String input = "invalid input";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }

    @Test
    public void allLinesMustStartAndEndWithPipeCharacter() {

        String input = "|name|address|~n|Patrick|patrick@test.com~n|Annie||~n";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }

    @Test
    public void fieldNamesMustBeUnique() {

        String input = "|name|address|address|~n|Patrick|patrick@test.com|~n|Annie||~n";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }

    @Test
    public void fieldNameMustNotBeEmpty() {

        String input = "|name||address|~n|Patrick|patrick@test.com||~n|Annie|||~n";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }

    @Test
    public void handlesEscapedPipeCharacter() {

        String input = "|name|address|~n|Patrick~|Kane|patrick@test.com|~n|Annie||~n";
        String expected = "2:2:1:address";

        String actual = sut.validate(input);

        assertEquals("output", expected, actual);
    }

    @Test
    public void handlesEscapedTildeCharacter() {

        String input = "|name|address|~n|abc~~~~ncd|efn|~n";
        String expected = "1:2:0:address";

        String actual = sut.validate(input);

        assertEquals("output", expected, actual);
    }

    @Test
    public void formatErrorWhenIvalidUseOfEscapeCharacter() {

        String input = "|name|address|~n|Patrick|patrick@~~~test.com||~n|Annie|||~n";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }

    @Test
    public void formatErrorWhenInvalidAsciiCharacter() {

        char c = (char) 229; // 'å'
        String input = "|abc" + String.valueOf(c) + "def|";
        String expected = FORMAT_ERROR;

        String actual = sut.validate(input);

        assertEquals("should output format error", expected, actual);
    }
}
