package nbr1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class FinderTest {

    private Finder sut = new Finder();

    @Test
    public void findsListInTheMiddle() {

        List<String> listA = Arrays.asList("ab", "cd", "efg", "hi", "ab");
        List<String> listB = Arrays.asList("cd", "efg", "hi");

        int actual = sut.find(listA, listB);

        assertEquals("index", 1, actual);
    }

    @Test
    public void findsListAtTheBeginning() {

        List<String> listA = Arrays.asList("ab", "cd", "efg", "hi", "ab");
        List<String> listB = Arrays.asList("ab", "cd");

        int actual = sut.find(listA, listB);

        assertEquals("index", 0, actual);
    }

    @Test
    public void findsListAtTheEnd() {

        List<Character> listA = Arrays.asList('A', 'E', 'G', 'C', 'C', 'B');
        List<Character> listB = Arrays.asList('C', 'B');

        int actual = sut.find(listA, listB);

        assertEquals("index", 4, actual);
    }

    @Test
    public void findsFirstListWhenMoreExist() {

        List<Integer> listA = Arrays.asList(1, 3, 4, 5, 3, 2, 7, 4, 5, 3, 8);
        List<Integer> listB = Arrays.asList(4, 5, 3);

        int actual = sut.find(listA, listB);

        assertEquals("index", 2, actual);
    }

    @Test
    public void returnsListNotFound() {

        List<Integer> listA = Arrays.asList(1, 3, 4, 5, 3, 2, 7, 4, 5, 3, 8);
        List<Integer> listB = Arrays.asList(8, 2);

        int actual = sut.find(listA, listB);

        assertEquals("index", -1, actual);
    }
}
