package nbr2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CounterTest {

    private Counter sut = new Counter();

    @Test
    public void countsOneDigit() {

        String expected = "11";

        String actual = sut.lookAndSay(1);

        assertEquals(expected, actual);
    }

    @Test
    public void countsTwoDifferentDigits() {

        String expected = "1211";

        String actual = sut.lookAndSay(21);

        assertEquals(expected, actual);
    }

    @Test
    public void countsTwoEqualDigits() {

        String expected = "21";

        String actual = sut.lookAndSay(11);

        assertEquals(expected, actual);
    }

    @Test
    public void countsGroupsOfDigits() {

        String expected = "312211";

        String actual = sut.lookAndSay(111221);

        assertEquals(expected, actual);
    }

    @Test
    public void iteratesAndCounts() {

        String expected = "1211";

        String actual = sut.lookAndSay(11, 2);

        assertEquals(expected, actual);
    }

    @Test
    public void handlesLargeResult() {

        String expected = "11131221131211132221121113122113121113222115";

        String actual = sut.lookAndSay(55, 9);

        assertEquals(expected, actual);
    }
}
