package nbr3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConverterTest {

    private Converter sut = new Converter();

    @Test
    public void convertsToOneDigit() {

        String expected = "n";

        String actual = sut.convert(6);

        assertEquals(expected, actual);
    }

    @Test
    public void convertsToTwoDigits() {

        String expected = "a0";

        String actual = sut.convert(7);

        assertEquals(expected, actual);
    }

    @Test
    public void convertsToThreeDigits() {

        String expected = "a00";

        String actual = sut.convert(49);

        assertEquals(expected, actual);
    }

    @Test
    public void convertsToFourDigits() {

        String expected = "aast";

        String actual = sut.convert(422);

        assertEquals(expected, actual);
    }

    @Test
    public void convertsToManyDigits() {

        String expected = "atlassian";

        String actual = sut.convert(7792875);

        assertEquals(expected, actual);
    }

    @Test
    public void convertsMaxNumber() {

        String expected = "t0ialtttailssl0l";

        String actual = sut.convert(10000000000000l);

        assertEquals(expected, actual);
    }

}
