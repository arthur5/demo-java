package nbr4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RobotTest {

    private Robot sut = new Robot(10, 15);

    @Test
    public void returnsAllPositions() {

        String command = "PMLPMMMLPMLPMML";
        String expected = "0211000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void returnsHexadecimalNumbers() {

        String command = "PLPLPLPLPLPLPLPLPLPL";
        String expected = "A000000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void pickupResetsRobotToFirstPosition() {

        String command = "PMLPMMPL";
        String expected = "1100000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void willNotGoBeyondLastPosition() {

        String command = "PLPMMMMMMMMMMMMML";
        String expected = "1000000001";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void loweringABlockOnAFullPileDoesNothing() {

        String command = "PLPLPLPLPLPLPLPLPLPLPLPLPLPLPLPLPL";
        String expected = "F000000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void loweringWithoutABlockDoesNothing() {

        String command = "PLL";
        String expected = "1000000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }

    @Test
    public void ignoresInvalidCommands() {

        String command = "PMLPMQWERL";
        String expected = "0200000000";

        String actual = sut.execute(command);

        assertEquals(expected, actual);
    }
}
